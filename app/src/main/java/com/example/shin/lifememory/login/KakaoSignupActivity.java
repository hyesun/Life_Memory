package com.example.shin.lifememory.login;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.kakao.auth.ApiResponseCallback;
import com.kakao.auth.ErrorCode;
import com.kakao.kakaotalk.KakaoTalkService;
import com.kakao.kakaotalk.callback.TalkResponseCallback;
import com.kakao.kakaotalk.response.KakaoTalkProfile;
import com.kakao.network.ErrorResult;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.MeResponseCallback;
import com.kakao.usermgmt.response.model.UserProfile;
import com.kakao.util.helper.log.Logger;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import com.example.shin.lifememory.MainActivity;

/**
 * Created by shin on 2016-10-19.
 */
public class KakaoSignupActivity extends Activity
{

    @Override
    protected  void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // Home으로 넘길지 가입 페이지로 넘길지 판단하기 위해 requestMe 호출
        requestMe();
    }

    // 사용자의 상태를 알아봄
    protected void requestMe()
    {
        // 유저의 정보를 받아옴
        UserManagement.requestMe(new MeResponseCallback() {
            @Override
            public void onFailure(ErrorResult errorResult)
            {
                String message = "failed to get user info. msg = " + errorResult;
                Logger.d(message);

                ErrorCode result = ErrorCode.valueOf(errorResult.getErrorCode());
                if(result == ErrorCode.CLIENT_ERROR_CODE)
                    finish();
                else
                    redirectLoginActivity();
            }

            @Override
            public void onSessionClosed(ErrorResult errorResult)
            {
                redirectLoginActivity();
            }

            @Override
            public void onNotSignedUp()
            {

            }

            @Override
            public void onSuccess(UserProfile userProfile)
            {
                String id;
                String name;

                Logger.d("UserProfile : " + userProfile);
                requestProfile(userProfile);

                id = String.valueOf(userProfile.getId());
                name = userProfile.getNickname();
                insertToDatabase(id, name);

                redirectMainActivity();
            }
        });
    }

    protected void requestProfile(final UserProfile userProfile)
    {
        KakaoTalkService.requestProfile(new TalkResponseCallback<KakaoTalkProfile>() {
            @Override
            public void onNotKakaoTalkUser() {

            }

            @Override
            public void onSessionClosed(ErrorResult errorResult) {

            }

            @Override
            public void onNotSignedUp() {

            }

            @Override
            public void onSuccess(KakaoTalkProfile talkProfile) {
                final Map<String, String> properties = userProfile.getProperties();
                properties.put("nickname", talkProfile.getNickName());
                properties.put("profile_image", talkProfile.getProfileImageUrl());
                properties.put("thumbnail_image", talkProfile.getThumbnailUrl());
                UserManagement.requestUpdateProfile(new ApiResponseCallback<Long>() {
                    @Override
                    public void onSessionClosed(ErrorResult errorResult) {

                    }

                    @Override
                    public void onNotSignedUp() {

                    }

                    @Override
                    public void onSuccess(Long userId) {
                        userProfile.updateUserProfile(properties);
                        if (userProfile != null) {
                            userProfile.saveUserToCache();
                        }
                        Logger.d("succeeded to update user profile" + userProfile);
                    }
                }, properties);

            }
        });
    }

    private void insertToDatabase (String id, String name)
    {

        class InsertData extends AsyncTask<String, Void, String>
        {
            //ProgressDialog loading;

            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(String s)
            {
                super.onPostExecute(s);
            }

            @Override
            protected String doInBackground(String... params)
            {
                try
                {
                    String id = (String)params[0];
                    String name = (String)params[1];

                    Log.i("id", id);
                    Log.i("name", name);

                    String link = "http://1.224.172.107/insert_member.php";
                    String data = "id=" + "m" + id + "&" + "nickname=" + name;

                    URL url = new URL(link);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setDoOutput(true);
                    conn.setDoInput(true);

                    OutputStream outputStream = conn.getOutputStream();

                    outputStream.write(data.getBytes("UTF-8"));
                    outputStream.flush();

                    BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                    StringBuilder sb = new StringBuilder();
                    String line = null;

                    // Read Server Response
                    while((line = reader.readLine()) != null)
                    {
                        sb.append(line);
                        break;
                    }
                    return sb.toString();
                }
                catch(Exception e){
                    return new String("Exception: " + e.getMessage());
                }

            }
        }

        InsertData task = new InsertData();
        task.execute(id, name);
    }

    private void redirectMainActivity()
    {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    private void redirectLoginActivity()
    {
        final Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        finish();
    }
}
