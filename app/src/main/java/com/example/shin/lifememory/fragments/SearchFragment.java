package com.example.shin.lifememory.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.shin.lifememory.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import net.daum.mf.speech.api.TextToSpeechClient;
import net.daum.mf.speech.api.TextToSpeechListener;
import net.daum.mf.speech.api.TextToSpeechManager;

import static android.content.Context.MODE_PRIVATE;
import static com.example.shin.lifememory.fragments.FuctionFragment.KEY_PREFERENCE;

/**
 * Created by shin on 2016-11-05.
 */

public class SearchFragment extends Fragment implements TextToSpeechListener
{
    String myJSON;
    String memo;
    JSONArray jsonArray;

    private ListView listView;
    private ArrayAdapter<String> adapter;
    TextView tvTitle;
    Button btnOk;

    private static final String TAG = "TextToSpeech";
    private TextToSpeechClient ttsClient;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_search, container, false);

        // TO-DO
        TextToSpeechManager.getInstance().initializeLibrary(getContext());


        adapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), R.layout.black_list_item);
        listView = (ListView)rootView.findViewById(R.id.list);
        listView.setAdapter(adapter);
        tvTitle = (TextView)rootView.findViewById(R.id.tv_title);

        SharedPreferences prefJson = getActivity().getSharedPreferences(KEY_PREFERENCE, MODE_PRIVATE);
        myJSON = prefJson.getString("json", "");

        SharedPreferences prefMemo = getActivity().getSharedPreferences(KEY_PREFERENCE, MODE_PRIVATE);
        memo = prefMemo.getString("memo", "");
        Log.i("memo", memo);
        tvTitle.setText(memo + "결과");

        ttsPlay("안녕");

        Log.i("search_json", myJSON);

        try
        {
            JSONObject jsonObject = new JSONObject(myJSON);
            jsonArray = jsonObject.getJSONArray("result");

            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject json = jsonArray.getJSONObject(i);
                final String name = json.getString("name");
                adapter.add(name);
                ttsPlay(name);
            }

        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        btnOk = (Button)rootView.findViewById(R.id.btn_ok);
        btnOk.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Fragment fragment = new HomeFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();
            }
        });

        return rootView;
    }

    private void ttsPlay(String str)
    {
        ttsClient = new TextToSpeechClient.Builder().setApiKey("3dd62b880352138b6c22302adf31d603").setSpeechMode(TextToSpeechClient.NEWTONE_TALK_1).setSpeechSpeed(1.0).setSpeechVoice(TextToSpeechClient.VOICE_WOMAN_DIALOG_BRIGHT).setListener(this).build();
        ttsClient.setSpeechText(str);
        Log.i("ttsClient", ttsClient.getSpeechText());
        ttsClient.play();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        TextToSpeechManager.getInstance().finalizeLibrary();
    }

    private void handleError(int errorCode)
    {
        String errorText;
        switch (errorCode)
        {
            case TextToSpeechClient.ERROR_NETWORK:
                errorText = "네트워크 오류";
                break;
            case TextToSpeechClient.ERROR_NETWORK_TIMEOUT:
                errorText = "네트워크 지연";
                break;
            case TextToSpeechClient.ERROR_CLIENT_INETRNAL:
                errorText = "음성합성 클라이언트 내부 오류";
                break;
            case TextToSpeechClient.ERROR_SERVER_INTERNAL:
                errorText = "음성합성 서버 내부 오류";
                break;
            case TextToSpeechClient.ERROR_SERVER_TIMEOUT:
                errorText = "음성합성 서버 최대 접속시간 초과";
                break;
            case TextToSpeechClient.ERROR_SERVER_AUTHENTICATION:
                errorText = "음성합성 인증 실패";
                break;
            case TextToSpeechClient.ERROR_SERVER_SPEECH_TEXT_BAD:
                errorText = "음성합성 텍스트 오류";
                break;
            case TextToSpeechClient.ERROR_SERVER_SPEECH_TEXT_EXCESS:
                errorText = "음성합성 텍스트 허용 길이 초과";
                break;
            case TextToSpeechClient.ERROR_SERVER_UNSUPPORTED_SERVICE:
                errorText = "음성합성 서비스 모드 오류";
                break;
            case TextToSpeechClient.ERROR_SERVER_ALLOWED_REQUESTS_EXCESS:
                errorText = "허용 횟수 초과";
                break;
            default:
                errorText = "정의하지 않은 오류";
                break;
        }

        final String statusMessage = errorText + " (" + errorCode + ")";

        Log.i(TAG, statusMessage);

    }

    @Override
    public void onError(int code, String message)
    {
        handleError(code);

        ttsClient = null;
    }

    @Override
    public void onFinished()
    {
        int intSentSize = ttsClient.getSentDataSize();
        int intRecvSize = ttsClient.getReceivedDataSize();

        final String strInacctiveText = "onFinished() SentSize : " + intSentSize + " RecvSize : " + intRecvSize;

        Log.i(TAG, strInacctiveText);

        ttsClient = null;
    }
}

