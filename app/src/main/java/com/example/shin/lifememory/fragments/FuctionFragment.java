package com.example.shin.lifememory.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.shin.lifememory.gets.FunctionDBConnection;
import com.example.shin.lifememory.R;
import com.kakao.usermgmt.response.model.UserProfile;

import java.util.concurrent.ExecutionException;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by shin on 2016-11-05.
 */

public class FuctionFragment extends Fragment
{

    private EditText etMemo;
    private Button btnOk;

    String myJSON = "";

    public static final String KEY_PREFERENCE = "json_key";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_function, container, false);

        // TO-DO
        final String id = String.valueOf(UserProfile.loadFromCache().getId());

        etMemo = (EditText)rootView.findViewById(R.id.et_memo);

        btnOk = (Button)rootView.findViewById(R.id.btn_ok);
        btnOk.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String memo = etMemo.getText().toString();
                //String id = "m" + UserProfile.loadFromCache().getId();
                Log.i("memoContents", memo);

                if (!memo.matches(""))
                {
                    FunctionDBConnection db = new FunctionDBConnection();
                    try
                    {
                        myJSON = db.execute(id, memo).get();
                        if (!myJSON.matches(""))
                        {
                            Log.i("function_json", myJSON);
                            SharedPreferences prefJson = getActivity().getSharedPreferences(KEY_PREFERENCE, MODE_PRIVATE);
                            SharedPreferences.Editor editorJson = prefJson.edit();
                            editorJson.putString("json", myJSON);
                            editorJson.commit();

                            SharedPreferences prefMemo = getActivity().getSharedPreferences(KEY_PREFERENCE, MODE_PRIVATE);
                            SharedPreferences.Editor editorMemo = prefMemo.edit();
                            editorMemo.putString("memo", memo);
                            editorMemo.commit();

                            Toast.makeText(getActivity(), "작업을 수행합니다.", Toast.LENGTH_SHORT).show();
                            Fragment fragment = new SearchFragment();
                            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();
                            return;
                        }
                    }
                    catch (InterruptedException | ExecutionException e)
                    {
                        e.printStackTrace();
                    }

                    Toast.makeText(getActivity(), "작업을 수행합니다.", Toast.LENGTH_SHORT).show();
                    Fragment fragment = new HomeFragment();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();
                }
                else
                {
                    Toast.makeText(getActivity(), "수행할 작업을 입력해주세요.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return rootView;
    }

}
