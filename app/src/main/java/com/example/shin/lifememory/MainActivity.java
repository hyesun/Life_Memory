package com.example.shin.lifememory;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.shin.lifememory.fragments.CategoryFragment;
import com.example.shin.lifememory.login.LoginActivity;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.LogoutResponseCallback;
import com.kakao.usermgmt.response.model.UserProfile;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import com.example.shin.lifememory.fragments.HomeFragment;
import com.example.shin.lifememory.fragments.FuctionFragment;

/*
키 해시
import android.util.Log;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import java.security.MessageDigest;
import android.util.Base64;
import java.security.NoSuchAlgorithmException;
*/

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener
{
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;

    private TextView txtName;
    private ImageView imgThumbnail;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*
        키 해시
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
        */

        mTitle = mDrawerTitle = getTitle();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView)findViewById(R.id.nav_view);
        View navigationHeader = navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener(this);

        // NetworkOnMainThreadException 해결방안
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // 카카오톡 이름 불러오기
        txtName = (TextView) navigationHeader.findViewById(R.id.txt_name);
        String name = UserProfile.loadFromCache().getNickname();
        txtName.setText(name);

        // 카카오톡 프사 불러오기
        imgThumbnail = (ImageView) navigationHeader.findViewById(R.id.img_thumbnail);
        String path = UserProfile.loadFromCache().getThumbnailImagePath();
        Bitmap bitmap = getBitmap(path);
        imgThumbnail.setImageBitmap(bitmap);

        Fragment fragment = new HomeFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();

    }

    // URL을 Bitmap으로 변환
    private Bitmap getBitmap(String path)
    {
        URLConnection connection;
        Bitmap roundedBitmap;

        try {
            URL url = new URL(path);
            connection = (URLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);

            return getRoundedBitmap(bitmap);
        }
        catch (Exception e)
        {
            Log.i("ImageException", e.toString());
            return null;
        }
    }

    // Bitmap 이미지를 원형으로
    public static Bitmap getRoundedBitmap(Bitmap bitmap) {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.GRAY;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();

        return output;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        DrawerLayout drawer = (DrawerLayout)findViewById(R.id.drawer_layout);

        String title = getString(R.string.app_name);

        if (id == R.id.nav_home)
        {
            Fragment fragment = new HomeFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();

            title = "Life Memory";
        }

        else if (id == R.id.nav_category)
        {
            Fragment fragment = new CategoryFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();

            title = "카테고리";
        }

        else if (id == R.id.nav_function)
        {
            Fragment fragment = new FuctionFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();

            title = "기능";
        }

        else if (id == R.id.nav_logout)
        {
            UserManagement.requestLogout(new LogoutResponseCallback() {
                @Override
                public void onCompleteLogout() {
                    redirectLoginActivity();
                }
            });
        }

        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(title);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void redirectLoginActivity()
    {
        final Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        finish();
    }
}
