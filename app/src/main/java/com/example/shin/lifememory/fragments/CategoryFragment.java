package com.example.shin.lifememory.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import com.example.shin.lifememory.Items;
import com.example.shin.lifememory.Items.SubCategory;
import com.example.shin.lifememory.Items.SubCategory.ItemList;
import com.example.shin.lifememory.R;
import com.example.shin.lifememory.gets.GetCategoryName;
import com.example.shin.lifememory.gets.GetCategoryNo;
import com.kakao.usermgmt.response.model.UserProfile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by shin on 2016-11-03.
 */

public class CategoryFragment extends Fragment
{
    private LinearLayout mLiearListView;

    String rootJSON;
    String rootNoJSON;
    String secondJSON;
    String secondNoJSON;
    String thirdJSON;
    String rootNo;
    String secondNo;
    JSONArray rootArray = null;
    JSONArray secondArray = null;
    JSONArray thirdArray = null;

    private ArrayList<Items> rootCategory;
    private ArrayList<SubCategory>[] sub;
    private ArrayList<ItemList>[] item;

    boolean isFirstViewClick = false;
    boolean isSecondViewClick = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_category, container, false);

        // TO-DO
        mLiearListView = (LinearLayout)rootView.findViewById(R.id.linear_listview);

        setDataList();
        addData();

        return rootView;
    }

    private void setDataList()
    {
        final String id = "m" + UserProfile.loadFromCache().getId();

        // 디비에서 0번 카테고리 가져오기

        GetCategoryName name1 = new GetCategoryName();
        try
        {
            rootJSON = name1.execute(id, "0").get();
        }
        catch (InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        try
        {
            JSONObject rootObject = new JSONObject(rootJSON);
            rootArray = rootObject.getJSONArray("result");

            rootCategory = new ArrayList<>();

            for (int i = 0; i < rootArray.length(); i++)
            {
                JSONObject root = rootArray.getJSONObject(i);
                final String rootName = root.getString("category_name");
                Log.i("rootName", rootName);

                sub = new ArrayList[rootArray.length()];
                sub[i] = new ArrayList<>();

                rootCategory.add(new Items(rootName, sub[i]));

                // 디비에서 rootCategory의 번호 정보 가져오기
                GetCategoryNo no1 = new GetCategoryNo();
                try
                {
                    rootNoJSON = no1.execute(id, rootName).get();
                }
                catch (InterruptedException | ExecutionException e)
                {
                    e.printStackTrace();
                }
                Log.i("rootNoJSON", rootNoJSON.toString());

                rootNo = saveRootCategoryNo();
                Log.i("rootNo", rootNo);

                // rootCategory의 번호를 부모로 갖는 두 번째 카테고리 가져오기
                GetCategoryName name2 = new GetCategoryName();
                try
                {
                    secondJSON = name2.execute(id, rootNo).get();
                }
                catch (InterruptedException | ExecutionException e)
                {
                    e.printStackTrace();
                }
                Log.i("secondJSON", secondJSON.toString());

                JSONObject secondObject = new JSONObject(secondJSON);
                secondArray = secondObject.getJSONArray("result");

                for (int j = 0; j < secondArray.length(); j++)
                {
                    JSONObject second = secondArray.getJSONObject(j);
                    final String secondName = second.getString("category_name");
                    Log.i("secondName", secondName);
                    Log.i("sub[i]", sub[i].toString());

                    item = new ArrayList[secondArray.length()];
                    item[j] = new ArrayList<>();

                    Log.i("item[j]", item[j].toString());
                    sub[i].add(new SubCategory(secondName, item[j]));

                    // 디비에서 해당 카테고리의 번호 정보 가져오기
                    GetCategoryNo no2 = new GetCategoryNo();
                    try
                    {
                        secondNoJSON = no2.execute(id, secondName).get();
                    }
                    catch (InterruptedException | ExecutionException e)
                    {
                        e.printStackTrace();
                    }
                    secondNo = saveSecondCategoryNo();

                    GetCategoryName name3 = new GetCategoryName();
                    try
                    {
                        thirdJSON = name3.execute(id, secondNo).get();
                    }
                    catch (InterruptedException | ExecutionException e)
                    {
                        e.printStackTrace();
                    }


                    JSONObject thirdObject = new JSONObject(thirdJSON);
                    thirdArray = thirdObject.getJSONArray("result");

                    for (int k = 0; k < thirdArray.length(); k++)
                    {
                        JSONObject third = thirdArray.getJSONObject(k);
                        final String thirdName = third.getString("category_name");
                        Log.i("thirdName", thirdName);

                        item[j].add(new ItemList(thirdName));
                    }
                }
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    // DB에서 찾아온 정보를 변환
    private String saveRootCategoryNo()
    {
        int num = 0;
        try
        {
            JSONObject jsonObject = new JSONObject(rootNoJSON);
            num = jsonObject.getInt("category_no");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        return String.valueOf(num);
    }

    private String saveSecondCategoryNo()
    {
        int num = 0;
        try
        {
            JSONObject jsonObject = new JSONObject(secondNoJSON);
            num = jsonObject.getInt("category_no");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        return String.valueOf(num);
    }


    public void addData()
    {
        for (int i = 0; i < rootCategory.size(); i++)
        {
            LayoutInflater firstInflater = null;
            firstInflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View mFirstLinearView = firstInflater.inflate(R.layout.row_first, null);

            final TextView mRootName = (TextView)mFirstLinearView.findViewById(R.id.text_root);
            final RelativeLayout mLinearFirstArrow = (RelativeLayout)mFirstLinearView.findViewById(R.id.linear_first);
            final LinearLayout mLinearScroll = (LinearLayout)mFirstLinearView.findViewById(R.id.linear_scroll);

            if (isFirstViewClick == false)
            {
                mLinearScroll.setVisibility(View.GONE);
            }
            else
            {
                mLinearScroll.setVisibility(View.VISIBLE);
            }

            mLinearFirstArrow.setOnTouchListener(new View.OnTouchListener()
            {
                @Override
                public boolean onTouch(View v, MotionEvent event)
                {
                    if (isFirstViewClick == false)
                    {
                        isFirstViewClick = true;
                        mLinearScroll.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        isFirstViewClick = false;
                        mLinearScroll.setVisibility(View.GONE);
                    }
                    return false;
                }
            });

            Items items = rootCategory.get(i);
            final String name = items.getcName();
            mRootName.setText(name);


            for (int j = 0; j < items.getmSubCategoryList().size(); j++)
            {
                LayoutInflater secondInflater = null;
                secondInflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                View mSecondLinearView = secondInflater.inflate(R.layout.row_second, null);

                final TextView mSubItemName = (TextView)mSecondLinearView.findViewById(R.id.text_second);
                final RelativeLayout mLinearSecondArrow = (RelativeLayout)mSecondLinearView.findViewById(R.id.linear_second);
                final LinearLayout mLinearSecondScroll = (LinearLayout)mSecondLinearView.findViewById(R.id.linear_scroll_second);

                if (isSecondViewClick == false)
                {
                    mLinearSecondScroll.setVisibility(View.GONE);
                }
                else
                {
                    mLinearSecondScroll.setVisibility(View.VISIBLE);
                }

                mLinearSecondArrow.setOnTouchListener(new View.OnTouchListener()
                {
                    @Override
                    public boolean onTouch(View v, MotionEvent event)
                    {
                        if (isSecondViewClick == false)
                        {
                            isSecondViewClick = true;
                            mLinearSecondScroll.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            isSecondViewClick = false;
                            mLinearSecondScroll.setVisibility(View.GONE);
                        }
                        return false;
                    }
                });

                SubCategory subCategory = items.getmSubCategoryList().get(j);
                final String catName = subCategory.getcSubName();
                mSubItemName.setText(catName);

                for (int k = 0; k < subCategory.getmItemList().size(); k++)
                {
                    LayoutInflater thirdInflater = null;
                    thirdInflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    View mThirdLinearView = thirdInflater.inflate(R.layout.row_third, null);

                    final TextView mItemName = (TextView)mThirdLinearView.findViewById(R.id.text_third);
                    final String itemName = subCategory.getmItemList().get(k).getItemName();
                    mItemName.setText(itemName);

                    mLinearSecondScroll.addView(mThirdLinearView);
                }

                mLinearScroll.addView(mSecondLinearView);
            }

            mLiearListView.addView(mFirstLinearView);
        }
    }

}
