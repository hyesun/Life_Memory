package com.example.shin.lifememory;

import java.util.ArrayList;

/**
 * Created by shin on 2016-11-04.
 */

public class Items {
    private String cName;
    private ArrayList<SubCategory> mSubCategoryList;

    public Items(String cName, ArrayList<SubCategory> mSubCategoryList) {
        super();
        this.cName = cName;
        this.mSubCategoryList = mSubCategoryList;
    }

    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }

    public ArrayList<SubCategory> getmSubCategoryList() {
        return mSubCategoryList;
    }

    public void setmSubCategoryList(ArrayList<SubCategory> mSubCategoryList) {
        this.mSubCategoryList = mSubCategoryList;
    }

    public static class SubCategory {
        private String cSubName;
        private ArrayList<ItemList> mItemList;

        public SubCategory(String cSubName, ArrayList<ItemList> mItemList) {
            super();
            this.cSubName = cSubName;
            this.mItemList = mItemList;
        }

        public String getcSubName() {
            return cSubName;
        }

        public void setcSubName(String cSubName) {
            this.cSubName = cSubName;
        }

        public ArrayList<ItemList> getmItemList() {
            return mItemList;
        }

        public void setmItemList(ArrayList<ItemList> mItemList) {
            this.mItemList = mItemList;
        }

        public static class ItemList {
            private String itemName;

            public ItemList(String itemName) {
                super();
                this.itemName = itemName;
            }

            public String getItemName() {
                return itemName;
            }

            public void setItemName(String itemName) {
                this.itemName = itemName;
            }
        }
    }
}
